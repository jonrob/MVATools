
#pragma once

// Include files
#include <string>
#include <map>
#include <vector>
#include <iostream>
#include <memory>

// ROOT
#include "TTree.h"
#include "TChain.h"
#include "TBranch.h"
#include "TTreeFormula.h"

/** @class NTupleReader NTupleReader.h B2D0hD2KsPiPiDalitz/NTupleReader.h
 *  
 *
 *  @author Chris Jones
 *  @date   2013-01-11
 */
class NTupleReader final
{
public: 

  /// TTree constructor
  NTupleReader( TTree * tree,
                const std::vector<std::string>& inputs );

  /// TChain constructor
  NTupleReader( TChain * chain,
                const std::vector<std::string>& inputs );

public:
  
  double variableDouble( const std::string& name ) const;

  float variableFloat( const std::string& name ) const;

  int variableInt( const std::string& name ) const;

  inline bool variableBool( const std::string& name ) const
  {
    return variableInt(name) != 0;
  }

  inline double variable( const std::string& name ) const
  {
    return variableDouble(name);
  }

  inline bool isOK() const { return m_OK; }

  template <class TYPE>
  void addVariable( TYPE * tree,
                    const std::string& name );

private:

  template <class TYPE>
  void initialise( TYPE * tree );

private:

  std::vector<std::string> m_inputs;

  std::map< std::string, std::unique_ptr<TTreeFormula> > m_formulas;

  bool m_OK;

  unsigned int m_index;

};
