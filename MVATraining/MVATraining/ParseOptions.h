
#pragma once

// Include files
#include "MVATraining/argvparser.h"

// Boost
#include "boost/lexical_cast.hpp"

/** @class ParseOptions ParseOptions.h
 *  
 *  Parse command line options
 *
 *  @author Chris Jones
 *  @date   2012-09-20
 */
class ParseOptions final
{

public:

  /// Is an option set
  bool isSet( const std::string & name ) const
  {
    return m_cmd.foundOption(name);
  }

  /// Access a parameter
  template < class TYPE >
  inline TYPE parameter( const std::string & name ) const
  {
    return ( boost::lexical_cast<TYPE>(m_cmd.optionValue(name)) );
  }

  /// Is all OK
  inline bool ok() const noexcept { return m_OK; }

  /// Define an option
  inline void defineOption( const std::string & option,
                            const std::string & defValue,
                            const std::string & description )
  {
    m_cmd.defineOption( option, defValue, description,
                        CommandLineProcessing::ArgvParser::OptionRequiresValue );
  }

  /// Parse the options
  bool parse( int argc, char** argv );

private:

  /// Options parser
  CommandLineProcessing::ArgvParser m_cmd;

  /// Is all OK
  bool m_OK{false};

};
