
#pragma once

// STL
#include <string>
#include <map>

/** @class Parameters Parameters.h MVATraining/Parameters.h
 *
 *  Class to handle application parameters.
 *
 *  @author Chris Jones
 *  @date   2013-11-28
 */
class Parameters final
{

public:

  /// Standard constructor
  explicit Parameters( const std::string& paramsFile = "" )
  {
    decode( paramsFile );
  }

  /// Decode the parameters
  bool decode( const std::string& paramsFile );

  /// The decoding status
  inline bool status() const noexcept { return m_status; }

  template < class TYPE >
  inline TYPE getParam( const std::string & name, const TYPE defValue )
  {
    const auto i = m_params.find(name);
    return ( i != m_params.end() ?
             boost::lexical_cast<TYPE>( i->second ) :
             defValue );
  }

private:

  /// Internal paramters mapping
  typedef std::map<std::string,std::string> ParamMap;

private:

  /// Parameters file
  std::string m_paramsFile;

  /// Map of decoded parameters
  ParamMap m_params;

  /// Status of the parameter decoding
  bool m_status{false};

};
