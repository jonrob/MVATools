
#pragma once

#include <string>
#include <memory>

#include "TChain.h"

class TChainCreator final
{
public:
  std::unique_ptr<TChain> chainRootFiles( const std::string & rootFilesName,
                                          const std::string & treeName = "" );
private:
  bool addFile( TChain * tree, const std::string & rootFileName );
private:
  std::string m_treeName;
};

/// Loads the root files listed in a text file and chains them
inline std::unique_ptr<TChain> chainRootFiles( const std::string & rootFilesName,
                                               const std::string & treeName = "" )
{
  TChainCreator creator;
  return creator.chainRootFiles(rootFilesName,treeName);
}
