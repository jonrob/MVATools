
#pragma once

// STL
#include <string>
#include <map>
#include <iostream>
#include <fstream>

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"

// local
#include "MVATraining/GetTime.h"

/** @class DecodeInputs DecodeInputs.h MVATraining/DecodeInputs.h
 *
 *  Simple class to decode input variables for MVA Training
 *
 *  @author Chris Jones
 *  @date   2013-11-27
 */

template < class NODETYPE >
class DecodeInputs final
{

public:

  /// Type for list of inputs
  using Inputs = std::vector<std::string>;

  /// Type for formulas
  using Formulas = std::vector<std::string>;

private:

  /// Data storage for each input
  class InData final
  {
  public:
    InData( ) = default;
    InData( const std::string& _formula, 
            NODETYPE    _type )
      : type(_type), formula(_formula) { }
  public:
    NODETYPE type;
    std::string formula;
  };

  /// mapping type
  using NodeMap = std::map<std::string,InData>;

public:

  /// Standard constructor
  explicit DecodeInputs( const std::string& inputsFile = "" )
  {
    decode(inputsFile);
  }

  /// Destructor
  ~DecodeInputs( ) = default;

  /// Decode the inputs
  bool decode( const std::string& inputsFile )
  {

    // Reset
    m_inputsFile = inputsFile;
    m_inputs.clear();
    m_formulas.clear();
    m_nodeMap.clear();
    m_status = true;

    if ( !m_inputsFile.empty() )
    {

      // Open inputs file
      std::ifstream inputList( m_inputsFile.c_str() );
      if ( !inputList.is_open() )
      {
        std::cerr << getTime() 
                  << " -> ERROR : Failed to open " << m_inputsFile 
                  << std::endl;
        m_status = false;
      }
      else
      {
        // read the inputs
        std::cout << getTime() 
                  << "Reading inputs file '" << inputsFile << "'" 
                  << std::endl;
        std::string input;
        while ( std::getline(inputList,input) )
        {
          if ( !input.empty() && input.find("#") == std::string::npos )
          {
            boost::regex expr("(.*);(.*);(.*)");
            boost::cmatch matches;
            if ( boost::regex_match( input.c_str(), matches, expr ) )
            {
              std::string name = matches[1];
              boost::erase_all(name," ");
              std::string type = matches[2];
              boost::erase_all(type," ");
              std::string var  = matches[3];
              boost::erase_all(var," ");
              std::cout << getTime() 
                        << " -> Name='" << name << "' NodeType='" << type << "'"
                        << " Formula='" << var << "'"
                        << std::endl;
              m_inputs.push_back(name);
              m_formulas.push_back(var);
              m_nodeMap[name] = InData( var, boost::lexical_cast<NODETYPE>(type) );
            }
            else
            {
              std::cerr << "ERROR : Failed to parse " << input << std::endl;
              m_status = false;
            }
          }
        }

        inputList.close();
      }

    }
    else
    {
      m_status = false;
    }

    return m_status;
  }

  /// The decoding status
  inline bool status() const noexcept { return m_status; }

  /// access the list of inputs
  inline const Inputs & inputs() const noexcept { return m_inputs; }

  /// access the list of formulas
  inline const Formulas & formulas() const noexcept { return m_formulas; }

  /// Access the node type for a given input
  bool nodeType( const std::string& input, NODETYPE& type ) const
  {
    const auto iN = m_nodeMap.find(input);
    if ( iN == m_nodeMap.end() )
    {
      std::cerr << getTime() << "Unknown variable " << input << std::endl;
      type = 0;
      return false;
    }
    type = iN->second.type;
    return true;
  }

  /// Access the node formula for a given input
  bool nodeFormula( const std::string& input, std::string& formula ) const
  {
    const auto iN = m_nodeMap.find(input);
    if ( iN == m_nodeMap.end() )
    {
      std::cerr << getTime() << "Unknown variable " << input << std::endl;
      formula = "";
      return false;
    }
    formula = iN->second.formula;
    return true;
  }

private:

  /// Text file containing the list of inputs
  std::string m_inputsFile;

  /// List of inputs
  Inputs m_inputs;

  /// List of formulas
  Formulas m_formulas;

  /// Mapping between variable and type
  NodeMap m_nodeMap;

  /// Status of the variable decoding
  bool m_status;

};
