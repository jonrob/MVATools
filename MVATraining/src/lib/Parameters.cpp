
// STL
#include <iostream>
#include <fstream>

// Boost
#include <boost/regex.hpp>
#include "boost/lexical_cast.hpp"
#include "boost/algorithm/string.hpp"

// local
#include "MVATraining/Parameters.h"
#include "MVATraining/GetTime.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Parameters
//
// 2013-11-28 : Chris Jones
//-----------------------------------------------------------------------------

bool Parameters::decode( const std::string& paramsFile )
{
  m_status = true;
  m_paramsFile = paramsFile;
  m_params.clear();

  if ( !paramsFile.empty() )
  {

    std::ifstream paramsStream( m_paramsFile.c_str() );
    if ( !paramsStream.is_open() )
    {
      std::cerr << getTime() 
                << " -> ERROR : Failed to open " << m_paramsFile 
                << std::endl;
      m_status = false;
    }
    else
    {

      // read the training parameters
      std::string param;
      std::cout << getTime() 
                << "Reading parameters file '" << paramsFile << "'" 
                << std::endl;
      while ( std::getline(paramsStream,param) )
      {
        if ( !param.empty() && param.find("#") == std::string::npos )
        {
          if ( param.find(" = ") != std::string::npos )
          {
            boost::regex expr("(.*) = (.*)");
            boost::cmatch matches;
            if ( boost::regex_match( param.c_str(), matches, expr ) )
            {
              std::string type = matches[1];
              boost::erase_all(type," ");
              std::string val  = matches[2];
              boost::erase_all(val," ");
              std::cout << getTime() << " -> " << type << " = " << val << std::endl;
              m_params[type] = val;
            }
            else
            {
              std::cerr << getTime() 
                        << "ERROR : Failed to parse '" << param << "'" 
                        << std::endl;
              m_status = false;
            }
          }
          else
          {
            std::cerr << getTime() 
                      << "ERROR : Failed to locate ' = ' operator in '" << param << "'" 
                      << std::endl;
            m_status = false;
          }
        }
      }
      paramsStream.close();

    }
    
  }
  else
  {
    m_status = false;
  }
  
  return m_status;
}
