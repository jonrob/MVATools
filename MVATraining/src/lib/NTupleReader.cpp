// Include files 

// STL
#include <sstream>

// local
#include "MVATraining/NTupleReader.h"

// STL
#include <algorithm>
#include <cmath>
#include <exception>

//-----------------------------------------------------------------------------
// Implementation file for class : NTupleReader
//
// 2013-01-11 : Chris Jones
//-----------------------------------------------------------------------------

template <class TYPE>
void NTupleReader::addVariable ( TYPE * tree, const std::string& name )
{
  if ( m_formulas.find(name) == m_formulas.end() )
  {
    std::ostringstream var;
    var << "var" << m_index++;
    m_formulas[name] = std::make_unique<TTreeFormula>( var.str().c_str(),
                                                       name.c_str(),
                                                       tree );
  }
}

template <class TYPE>
void NTupleReader::initialise( TYPE * tree )
{
  // Loop over variables and create a formula object for each one
  for ( auto& input : m_inputs ) { addVariable( tree, input ); }
}

NTupleReader::NTupleReader( TTree * tree,
                            const std::vector<std::string>& inputs )
  : m_inputs(inputs), m_OK(true), m_index(0)
{
  initialise(tree);
}

NTupleReader::NTupleReader( TChain * chain,
                            const std::vector<std::string>& inputs )
  : m_inputs(inputs), m_OK(true), m_index(0)
{
  initialise(chain);
}

// Get a variable as double
double NTupleReader::variableDouble( const std::string& name ) const
{
  const auto i = m_formulas.find(name);
  if ( i == m_formulas.end() )
  {
    std::cerr << "WARNING : Could not compute variable " << name << std::endl;
  }
  return ( i != m_formulas.end() ? i->second->EvalInstance() : -1001 );
}

// Get a variable as int
int NTupleReader::variableInt( const std::string& name ) const
{
  const auto i = m_formulas.find(name);
  if ( i == m_formulas.end() )
  {
    std::cerr << "WARNING : Could not compute variable " << name << std::endl;
  }
  return (int) ( i != m_formulas.end() ? i->second->EvalInstance() : -1001 );
}

// Get a variable as float
float NTupleReader::variableFloat( const std::string& name ) const
{
  return (float) variableDouble(name);
}

//=============================================================================
