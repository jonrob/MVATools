
// STL
#include <iostream>
#include <string>

// local
#include "MVATraining/ParseOptions.h"

bool ParseOptions::parse( int argc, char** argv )
{
  
  // Define error codes
  m_cmd.addErrorCode(0, "Success");
  m_cmd.addErrorCode(1, "Error");

  // Help
  m_cmd.setHelpOption("h", "help", "Print the help");
 
  // finally parse and handle return codes (display help etc...)
  const auto result = m_cmd.parse(argc,argv);
  if ( result != CommandLineProcessing::ArgvParser::NoParserError )
  {
    std::cerr << m_cmd.parseErrorDescription(result) << std::endl;
    m_OK = false;
  }
  else
  {
    m_OK = true;
  }

  return m_OK;
}
