
// STL
#include <time.h>

// local
#include "MVATraining/GetTime.h"

//-----------------------------------------------------------------------------
// Implementation file for class : GetTime
//
// 2013-11-28 : Chris Jones
//-----------------------------------------------------------------------------

// get time string
const std::string getTime()
{
  const time_t now = time(0);
  struct tm   tstruct;
  static char buf[80];
  tstruct = *localtime(&now);
  // Visit http://www.cplusplus.com/reference/clibrary/ctime/strftime/
  // for more information about date/time format
  strftime( buf, sizeof(buf), "%Y-%m-%d %X | ", &tstruct );
  return buf;
}
