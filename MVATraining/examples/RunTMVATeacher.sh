#!/bin/bash

# Data files
export SIGNALFILE="signal.root:B2MuMuTuple/DecayTree"
export BCKGRDFILE="background.root:B2MuMuTuple/DecayTree"

mkdir output
export WORKDIR="DataSet/output/weights/"
mkdir -p $WORKDIR
cp TMVAInputs.txt $WORKDIR

# Run the teacher
TMVATeacher.exe --TrainingParameters TMVATraining.txt --Inputs TMVAInputs.txt --SignalTrainingFile $SIGNALFILE --BckgTrainingFile $BCKGRDFILE 2>&1 | tee TMVATrain.log

exit 0
