#!/bin/bash

# Data files
#export ROOTDIR=/castor/cern.ch/user/j/jonrob/NeuroBayesSelTeacher/example
export ROOTDIR=/afs/cern.ch/work/j/jonrob/MVATraining/example
export SIGNALFILE=${ROOTDIR}/signal.root
export BCKGRDFILE=${ROOTDIR}/background.root

# Run the teacher
NBTeacher.exe --SignalTreeName "B2MuMuTuple/DecayTree" --BckgTreeName "B2MuMuTuple/DecayTree" --TrainingParameters NBTraining.txt --Inputs NBInputs.txt --SignalTrainingFile $SIGNALFILE --BckgTrainingFile $BCKGRDFILE --OutputExpertiseFile expert.nb 2>&1 | tee NBTrain.log

# Analysis the training
NBAsciiToRoot.exe
NBAnalysis.exe teacherHistos.root NBTrain.pdf sort correl_signi.txt
rm -f ahist.txt

exit 0
