
# This is a comment line !
# Blank lines are also ignored.

BsTau       ; 14 ; B_s0_TAU
MinMuIPChi2 ; 14 ; sqrt(TMath::Min(muplus_MINIPCHI2,muminus_MINIPCHI2))
BsIso       ; 19 ; B_s0_D1_isolation_Giampi+B_s0_D2_isolation_Giampi
BsPt        ; 14 ; B_s0_PT
BsCDFiso    ; 14 ; B_s0_CDFiso
MinMuPt     ; 14 ; TMath::Min(muplus_PT,muminus_PT)
BsCosnk     ; 14 ; B_s0_cosnk
BsDOCA      ; 14 ; B_s0_doca
BsIP        ; 14 ; B_s0_IP_OWNPV
